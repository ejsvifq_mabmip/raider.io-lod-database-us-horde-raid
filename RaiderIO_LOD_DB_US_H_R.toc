## Interface: 80205
## Author: cqwrteur
## Title: Raider.IO LOD Database US Horde Raid
## Version: @project-version@
## LoadOnDemand: 1
## X-RAIDER-IO-LOD: 1
## X-RAIDER-IO-LOD-FACTION: Horde

db/db_raiding_us_horde_characters.lua
db/db_raiding_us_horde_lookup.lua
